const express = require('express');
const hbs = require('hbs');
const app = express();
const fs = require('fs');

const port = process.env.PORT || 3000;

hbs.registerPartials(__dirname + '/views/partials');

app.use((req, res, next) => {
	var now = new Date().toString();
	var log = (`${now}: ${req.method} ${req.url}`);

	console.log(log);
	fs.appendFile('server.log', log + '\n', (err) => {
  		if (err) throw err;
  			console.log('The "data to append" was appended to file!');
		});
	next();
});

// app.use((req, res, next) => {
// 	res.render('maintenance.hbs', {
// 		pageTitle: 'Maintenance mode',
// 		message: 'This site is in maintenance mode. Please check back soon.'
// 	});
// });

app.use(express.static(__dirname + '/public')); 

hbs.registerHelper('getCurrentYear', () => {
	return new Date().getFullYear();
});

hbs.registerHelper('screamIt', (text) => {
	return text.toUpperCase();
});

app.get('/', (req, res) => {
	res.render('home.hbs', {
		pageTitle: 'Home Page',
		welcomeMessage: 'Welcome to CompleteFarmer',
	});
});

app.get('/about', (req, res) => {
	res.render('about.hbs', {
		pageTitle: 'About Page',			
	});
});

app.get('/bad', (req, res) => {
	res.send({
		errorMessage: 'Error handling this request'
	});
});

app.listen(port, () => {
	console.log(`Server is up on port ${port}`);
});