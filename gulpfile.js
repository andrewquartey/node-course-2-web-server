const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const nodemon = require('gulp-nodemon');

gulp.task('default', ['browser-sync'], function () {
});

gulp.task('browser-sync', ['nodemon'], (done) => {
  browserSync.init(null, {
    proxy: "http://localhost:3000",
        files: ["public/**/*.*", "views/**/*.*", "views/partials/footer.hbs"],
        // browser: "google chrome",
        port: 5000,
  });
  done();
});

gulp.task('nodebaby', (cb) => {
  
  var started = false;
  
  return nodemon({
    script: 'server.js'
  }).on('start', function () {
    // to avoid nodemon being started multiple times
    // thanks @matthisk
    if (!started) { 
      cb();   
      started = true;      
    } 
  });
});

gulp.task('nodemon',['nodebaby'] , (done) => {
  browserSync.reload();
  done();
});